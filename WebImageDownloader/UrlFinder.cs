﻿using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace WebImageDownloader
{
    static class UrlFinder
    {
        public static List<string> Find(string htmlPage)
        {
            Regex regex = new Regex(@"<img.+src ?= ?[""']{0,2}(?<url>.+?)[""']", RegexOptions.IgnoreCase | RegexOptions.Compiled);
            MatchCollection collection = regex.Matches(htmlPage);

            List<string> paths = new List<string>();
            foreach (Match item in collection)
            {
                string url = item.Groups["url"].Value;
                
                if (Regex.IsMatch(url, @"^http.+\.[\w\d]{2,4}$", RegexOptions.IgnoreCase))
                {
                    paths.Add(url);
                }
            }
            return paths;
        }
    }
}
