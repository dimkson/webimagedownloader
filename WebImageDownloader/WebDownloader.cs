﻿using System;
using System.Collections.Generic;
using System.Net;
using System.IO;

namespace WebImageDownloader
{
    class WebDownloader: IDisposable
    {
        private WebClient _webClient;

        public WebDownloader()
        {
            _webClient = new WebClient();
        }

        public void Dispose()
        {
            _webClient.Dispose();
        }

        public string DownloadHtml(string url)
        {
            return _webClient.DownloadString(url);
        }

        public void DownloadImage(IEnumerable<string> paths, string toPath)
        {
            if (!Directory.Exists(toPath))
                Directory.CreateDirectory(toPath);

            foreach (string path in paths)
            {
                try
                {
                    _webClient.DownloadFile(path, Path.Combine(toPath, Path.GetFileName(path)));
                    Console.WriteLine($"{path} Success");
                }
                catch (WebException)
                {
                    Console.WriteLine($"Не удалось скачать файл по адресу {path}");
                }
            }
            Console.WriteLine("Скачивание завершено!");
        }
    }
}
