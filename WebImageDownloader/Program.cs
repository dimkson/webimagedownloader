﻿using System;
using System.Collections.Generic;

namespace WebImageDownloader
{
    class Program
    {
        private static void Main(string[] args)
        {
            WebDownloader webDownloader = new WebDownloader();

            try
            {
                string htmlPage = webDownloader.DownloadHtml("https://otus.ru");

                List<string> paths = UrlFinder.Find(htmlPage);

                webDownloader.DownloadImage(paths, "Images");
            }
            catch (Exception exc)
            {
                Console.WriteLine(exc.Message);
            }

            Console.ReadLine();
        }
    }
}
